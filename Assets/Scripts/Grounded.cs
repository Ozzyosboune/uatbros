﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grounded : MonoBehaviour
{
    public PlayerController player;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Floor>())
        {
            player.isGround = true;
            player.isJump = false;
            player.isdJump = true;
            player.jText.text = "2";
        }
    }

    void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Floor>())
        {
            player.isGround = true;
            player.isJump = false;
            player.isdJump = true;
        }
    }

    void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.GetComponent<Floor>())
        {
            player.isGround = false;
            player.isJump = true;
            player.isdJump = true;
            player.jText.text = "1";
        }
    }
}
