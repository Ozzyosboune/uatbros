﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public Transform pauseMenu;
    public Transform vicCan;
    public Transform gamOverCan;

    public Vector3 lastCheckpoint;

    public int playerLives;
    public int maxLives;

    public PlayerController player;

    public bool isPaused = false;

    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (playerLives < 0)
        {
            gamOverCan.gameObject.SetActive(true);
            isPaused = true;
            Destroy(player.gameObject);
        }
    }

    public void TakeLife()
    {
        playerLives--;
    }
}
