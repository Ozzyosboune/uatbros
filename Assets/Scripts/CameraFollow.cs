﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject player;

    private Transform tf;
    private Vector3 offset;
    
    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        offset = tf.position - player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        tf.position = player.transform.position + offset;
    }
}
