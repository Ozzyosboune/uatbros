﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();
        GameManager.instance.player = player;

        if (collision.gameObject.GetComponent<PlayerController>())
        {
            player.Respawn();
            GameManager.instance.TakeLife();
        }
    }
}
