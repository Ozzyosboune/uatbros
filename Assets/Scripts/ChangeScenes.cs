﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScenes : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void LoadScene(int load)
    {
        SceneManager.LoadScene(load);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        GameManager.instance.vicCan.gameObject.SetActive(true);
        GameManager.instance.isPaused = true;
    }
}
