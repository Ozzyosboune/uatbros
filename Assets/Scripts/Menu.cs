﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Hello");
            Pause();
        }
    }

    public void Pause()
    {
        GameManager.instance.pauseMenu.gameObject.SetActive(true);
        GameManager.instance.isPaused = true;
        Time.timeScale = 0;
    }

    public void Resume()
    {
        GameManager.instance.pauseMenu.gameObject.SetActive(false);
        GameManager.instance.isPaused = false;
        Time.timeScale = 1;
    }

    public void LoadScene(string load)
    {
        SceneManager.LoadScene(load);
        GameManager.instance.playerLives = GameManager.instance.maxLives;
        Time.timeScale = 1;
        GameManager.instance.isPaused = false;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
