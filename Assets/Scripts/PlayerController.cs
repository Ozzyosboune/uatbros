﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    public float mSpeed;

    public float jSpeed;

    [SerializeField]
    private AnimationClip idle;
    [SerializeField]
    private AnimationClip walk;
    [SerializeField]
    private AnimationClip jump;

    public Text jText;

    private Transform tf;
    private Rigidbody2D rb;
    private Animator anim;

    public bool isGround = true;
    public bool isJump = false;
    public bool isdJump = true;

    private bool facingRight;
    private bool isMoving = false;
    // Use this for initialization
    void Start()
    {
        tf = GetComponent<Transform>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        GameManager.instance.lastCheckpoint = this.gameObject.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.isPaused)
        {
            if (Input.GetKey(KeyCode.A))
            {
                tf.position -= tf.right * mSpeed * Time.deltaTime;
                isMoving = true;
                if (!facingRight)
                {
                    Flip();
                    facingRight = true;
                }
            }
            if (Input.GetKeyUp(KeyCode.A))
            {
                isMoving = false;
            }
            if (Input.GetKey(KeyCode.D))
            {
                tf.position += tf.right * mSpeed * Time.deltaTime;
                isMoving = true;
                if (facingRight)
                {
                    Flip();
                    facingRight = false;
                }
            }
            if (Input.GetKeyUp(KeyCode.D))
            {
                isMoving = false;
            }
            if (isGround)
            {
                if (Input.GetKeyDown(KeyCode.W))
                {
                    rb.AddForce(new Vector2(0, jSpeed), ForceMode2D.Impulse);
                    anim.Play(jump.name);
                }
            }
            if (isJump)
            {
                if (isdJump)
                {
                    if (Input.GetKeyDown(KeyCode.W))
                    {
                        rb.AddForce(new Vector2(0, jSpeed / 2), ForceMode2D.Impulse);
                        anim.Play(jump.name);
                        jText.text = "0";
                        isJump = false;
                        isdJump = false;
                    }
                }
            }
        }
        if (!isMoving)
        {
            anim.Play(idle.name);
        }
        if (!isJump)
        {
            if (isMoving)
            {
                anim.Play(walk.name);
            }
        }
    }

    public void Respawn()
    {
        this.gameObject.transform.position = GameManager.instance.lastCheckpoint;
    }

    void Flip()
    {
        facingRight = !facingRight;

        Vector3 scale = tf.localScale;
        scale.x *= -1;
        tf.localScale = scale;
    }
}
